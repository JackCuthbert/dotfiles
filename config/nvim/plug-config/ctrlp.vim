" CtrlP settings
let g:ctrlp_custom_ignore = '\v[\/](node_modules|tmp)|(\.(swp|ico|git|svn|DS_Store))$'
let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
let g:ctrlp_map = '<c-p>'
let g:ctrlp_show_hidden = 1
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_dont_split = 'nerdtree'
